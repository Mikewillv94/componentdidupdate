import { Component } from "react";
import CharacterList from "./components/character-list";
import "./App.css";

class App extends Component {
  state = {
    characterList: [],
    nextUrl: "https://rickandmortyapi.com/api/character/",
  };

  componentDidMount() {
    this.getCharacters(this.state.nextUrl);
  }

  getCharacters(url) {
    const { characterList } = this.state;
    fetch(url)
      .then((res) => res.json())
      .then((body) => {
        this.setState({
          // Fazendo o spread do estado atual e dos novos resultados e jogando tudo em novo array.
          characterList: [...characterList, ...body.results],
          nextUrl: body.info.next,
          // se não houver próxima página, o next será null
        });
      });
  }

  componentDidUpdate() {
    this.getCharacters(this.state.nextUrl);
  }

  render() {
    const { characterList } = this.state;

    return (
      <div className="App">
        <header className="App-header">
          <CharacterList list={characterList} />
        </header>
      </div>
    );
  }
}

export default App;
